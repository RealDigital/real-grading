<?php


namespace App\Services;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\RolePermission;
use App\Entity\User;
use App\Entity\UserRole;
use App\Entity\UserToken;
use App\Security\SecurityUtil;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    public $userRepository;
    public $entityManager;

    public function __construct(EntityManagerInterface  $em)
    {
        $this->entityManager = $em;
        $this->userRepository = $em->getRepository(User::class);
    }

    public function checkUsername($username)
    {
        $user = $this->userRepository->findOneBy([
            'userName' => $username
        ]);

        return $user? true : false;
    }

    public function checkLogin($username, $password)
    {
        $user = $this->userRepository->findOneBy([
            'userName' => $username
        ]);
        if (!$user){
            return false;
        }
        $password = SecurityUtil::passCrypt($password, $user->getSalt());
        if ($password == $user->getPassword())
        {
            $user_token = new UserToken();
            $user_token->setUser($user);
            $user_token->setExpiretime(\DateTime::createFromFormat(
                "Y-m-d H:i:s", date("Y-m-d H:i:s", strtotime('+1 day'))));
            $token = SecurityUtil::getToken($user->getUserName());
            $user_token->setUsertoken($token);
            $this->entityManager->persist($user_token);
            $this->entityManager->flush();
            return $token;
        }
        else
        {
            return false;
        }
    }
}