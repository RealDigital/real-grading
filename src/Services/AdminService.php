<?php


namespace App\Services;


use App\Entity\Course;
use App\Entity\CourseStudent;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\RolePermission;
use App\Entity\User;
use App\Entity\UserRole;
use App\Entity\UserToken;
use App\Security\SecurityUtil;
use Doctrine\ORM\EntityManagerInterface;

class AdminService extends UserService
{
    private $courseRepository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->courseRepository = $em->getRepository(Course::class);
    }

    public function register_user($username,$password,$str_role)
    {
        $this->entityManager->beginTransaction();
        try {

            $user = new User();
            $user_token = new UserToken();

            $user->setUsername($username);
            $user->setPassword($password);
            $user->setStatus(0);
            $user->setSalt(SecurityUtil::saltGen());
            $user->setPassword(SecurityUtil::passCrypt($password, $user->getSalt()));
            $user->setCreateTime(\DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s")));
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $role = new Role();
            $role->setName($str_role);
            $role->setDescription("This is a user");
            $this->entityManager->persist($role);
            $this->entityManager->flush();

            $userRole = new UserRole();
            $userRole->setRole($role);
            $userRole->setUser($user);
            $this->entityManager->persist($userRole);
            $this->entityManager->flush();

            $permission = new Permission();
            $permission->setName("something");
            $permission->setDescription("something");
            $this->entityManager->persist($permission);
            $this->entityManager->flush();

            $rolePermission = new RolePermission();
            $rolePermission->setRole($role);
            $rolePermission->setPermission($permission);
            $this->entityManager->persist($rolePermission);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();
            return true;

        } catch (\Exception $e) {
            $this->entityManager->rollBack();
            return false;

        }
    }

    public function register_course($course_name, $instructor_id, $ta_id)
    {
        $this->entityManager->beginTransaction();
        try {
            $course = new Course();
            $course->setCourseName($course_name);
            $course->setInstructor($instructor_id);
            $course->setTa($ta_id);
            $this->entityManager->persist($course);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            $this->entityManager->rollBack();
            return false;
        }
    }

    public function register_courseStudent($course_id, $student_id)
    {
        $this->entityManager->beginTransaction();
        try {
            $course_student = new CourseStudent();
            $course_student->setCourse($course_id);
            $course_student->setStudent($student_id);
            $this->entityManager->persist($course_student);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            $this->entityManager->rollBack();
            return false;
        }
    }

    public function searchUserById($user_id)
    {
        return $this->userRepository->findOneBy([
            'id' => $user_id
        ]);
    }

    public function searchUserByName($user_name)
    {
        return $this->userRepository->findBy([
            'userName' => $user_name
        ]);
    }

    public function searchCourseByName($course_name)
    {
        return $this->courseRepository->findOneBy([
            'courseName' => $course_name
        ]);
    }
}