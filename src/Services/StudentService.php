<?php


namespace App\Services;


use Doctrine\ORM\EntityManagerInterface;

class StudentService extends UserService
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /* students and instructors can access to the assignment if they have the permission */
    public function getAssignment()
    {

    }

    /* student submit an assignment
       create Grade() without grade given by instructor
    */
    public function submitAssignment()
    {

    }

    /* students and instructors can access to the grade if they have the permission */
    public function getGrade()
    {

    }
}