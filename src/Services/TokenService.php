<?php


namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\UserToken;

class TokenService
{
    private $tokenRepository;
    private $entityManager;

    public function __construct(EntityManagerInterface  $em)
    {
        $this->entityManager = $em;
        $this->tokenRepository = $em->getRepository(UserToken::class);
    }

    public function isExpired($userId, $token)
    {
        $userToken = $this->tokenRepository->findOneBy([
            'userid' => $userId,
            'usertoken' => $token,
        ]);

        $exp = $userToken->getExpiretime();
        $exp = $exp->format("y-m-d h:i:s");
        $now = date("y-m-d h:i:s");
        if (!$userToken){
            return true;
        }
        if(strtotime($now)<strtotime($exp)){
            return false;
        }else{
            return true;
        }

    }
}