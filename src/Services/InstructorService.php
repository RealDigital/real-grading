<?php


namespace App\Services;


use App\Entity\Assignment;
use App\Entity\Course;
use App\Entity\CourseStudent;
use App\Entity\Grade;
use Doctrine\ORM\EntityManagerInterface;

class InstructorService extends UserService
{
    private $courseRepository;
    private $courseStudentRepository;
    private $assignmentRepository;
    private $gradeRepository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->courseRepository = $em->getRepository(Course::class);
        $this->courseStudentRepository = $em->getRepository(CourseStudent::class);
        $this->assignmentRepository = $em->getRepository(Assignment::class);
        $this->gradeRepository = $em->getRepository(Grade::class);
    }

    public function listCourse($user_id)
    {
        $course =  $this->courseRepository->findBy([
            'instructor' => $user_id
        ]);
        if (!$course){
            return false;
        }
        return $course;
    }

    public function listStudent($course_id)
    {
        $courseStudent = $this->courseStudentRepository->findBy([
            'course' => $course_id
        ]);
        if (!$courseStudent){
            return false;
        }

        $student_id = array();
        foreach ($courseStudent as $item){
            $std_id = $courseStudent->getStudent();
            array_push($student_id, $std_id);
        }

        $student = array();
        foreach ($student_id as $item){
            $std = $this->userRepository->findOneBy([
                'id' => $item
            ]);
            array_push($student, $std);
        }
        return $student;
    }

    /* instructor create an assignment*/
    public function setAssignment($course_id, $title, $description, $deadline)
    {
        $this->entityManager->beginTransaction();
        try {
            $assignment = new Assignment();
            $assignment->setCoures($course_id);
            $assignment->setTitle($title);
            $assignment->setDescription($description);
            $assignment->setDeadline($deadline);
            $this->entityManager->persist($assignment);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            $this->entityManager->rollBack();
            return false;
        }
    }

    /* students and instructors can access to the assignment if they have the permission */
    public function getAssignment($assignment_id)
    {
        $assignment = $this->assignmentRepository->findOneBy([
            'id' => $assignment_id
        ]);
        if (!$assignment){
            return false;
        }
        return $assignment;
    }

    public function setGrade($assignment_id, $student_id)
    {
        $this->entityManager->beginTransaction();
        try {
            $grade = new Grade();
            $grade->setStudent($student_id);
            $grade->setAssignment($assignment_id);
            $grade->setSubTime(null);
            $grade->setGrade(null);
            $this->entityManager->persist($grade);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            $this->entityManager->rollBack();
            return false;
        }
    }

    /* instructor give a grade to the student*/
    public function giveGrade($assignment_id, $student_id, $point)
    {
        $grade = $this->gradeRepository->findOneBy([
            'assignment' => $assignment_id,
            'student' => $student_id
        ]);
        if (!$grade){
            return false;
        }
        $grade->setGrade($point);
        $this->entityManager->flush();
        return true;
    }

    /* students and instructors can access to the grade if they have the permission */
    public function getGrade($assignment_id, $student_id)
    {
        $grade = $this->gradeRepository->findOneBy([
            'assignment' => $assignment_id,
            'student' => $student_id
        ]);
        if (!$grade){
            return false;
        }
        return $grade;
    }
}