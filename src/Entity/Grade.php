<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grade
 *
 * @ORM\Table(name="grade", indexes={@ORM\Index(name="assignment_id", columns={"assignment_id"}), @ORM\Index(name="student_id", columns={"student_id"})})
 * @ORM\Entity
 */
class Grade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="grade", type="integer", nullable=false)
     */
    private $grade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sub_time", type="datetime", nullable=false)
     */
    private $subTime;

    /**
     * @var \Assignment
     *
     * @ORM\ManyToOne(targetEntity="Assignment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assignment_id", referencedColumnName="id")
     * })
     */
    private $assignment;

    /**
     * @var \CourseStudent
     *
     * @ORM\ManyToOne(targetEntity="CourseStudent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="student_id", referencedColumnName="student_id")
     * })
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrade(): ?int
    {
        return $this->grade;
    }

    public function setGrade(int $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getSubTime(): ?\DateTimeInterface
    {
        return $this->subTime;
    }

    public function setSubTime(\DateTimeInterface $subTime): self
    {
        $this->subTime = $subTime;

        return $this;
    }

    public function getAssignment(): ?Assignment
    {
        return $this->assignment;
    }

    public function setAssignment(?Assignment $assignment): self
    {
        $this->assignment = $assignment;

        return $this;
    }

    public function getStudent(): ?CourseStudent
    {
        return $this->student;
    }

    public function setStudent(?CourseStudent $student): self
    {
        $this->student = $student;

        return $this;
    }


}
