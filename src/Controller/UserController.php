<?php

namespace App\Controller;

use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private $userService;

    /**
     * @Route("/api/user/",methods={"GET"})
     */
    public function listUser()
    {
        return $this->json(["message" => "ok", "code" => 200]);
    }
}
