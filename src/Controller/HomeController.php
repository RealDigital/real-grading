<?php


namespace App\Controller;

use App\Security\Result;
use App\Services\UserService;
use App\Services\AdminService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/api/register", name="user",methods={"POST"})
     * @param Request $request
     * @param AdminService $adminService
     * @return JsonResponse
     */
    public function register(Request $request,AdminService $adminService)
    {
        $data = json_decode($request->getContent(), true);
        $username = $data["username"];
        $password = $data["password"];
        $role = $data["role"];

        if ($adminService->register_user($username, $password, $role))
        {
            return $this->json(Result::return(Result::SUCCESS));
        }
        else
        {
            return $this->json(Result::return(Result::ERROR));
        }
    }


    /**
     * @Route("/api/checkuser/{username}",methods={"GET","POST"})
     * @param $username
     * @param UserService $userService
     * @return JsonResponse
     */
    public function checkUsername($username, UserService $userService)
    {

        if ($userService->checkUsername($username)) {
            return $this->json(Result::return(Result::USERNAME_REPEAT));
        } else {
            return $this->json(Result::return(Result::SUCCESS));
        }

    }

    /**
     * @Route("/api/login",methods={"POST"})
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function checkLogin(Request $request, UserService $userService)
    {
        $data = json_decode($request->getContent(), true);
        $username = $data["username"];
        $password = $data["password"];

        $data = $userService->checkLogin($username,$password);
        if ($data) {
            return $this->json(Result::return(Result::SUCCESS,$data));
        } else {
            return $this->json(Result::return(Result::ERROR));
        }
    }
}