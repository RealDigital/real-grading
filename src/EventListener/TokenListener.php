<?php


namespace App\EventListener;

use App\Controller\TokenAuthenticatedController;
use App\Entity\UserToken;
use App\Services\TokenService;
use App\Services\UserService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class TokenListener
{
    private $tokens;
    private $userId;
    private $userService;
    private $tokenService;

    public function __construct($tokens,UserService $userService,TokenService $tokenService)
    {
        $this->tokens = $tokens;
        $this->userService = $userService;
        $this->tokenService = $tokenService;
    }

    private function checkToken($userId, $token)
    {
        return $this->tokenService->isExpired($userId,$token);
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {
            $this->tokens = $event->getRequest()->query->get('token');
            $this->userId = $event->getRequest()->query->get('userId');

            if ($this->checkToken($this->userId,$this->tokens))
            {
                $redirectUrl = "/";
                $event->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }


            if (!$this->userId){
                $redirectUrl = "/";
                $event->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }
            if (!in_array($this->tokens, $this->tokens)) {
                throw new AccessDeniedHttpException('This action needs a valid token!');

            }
        }
    }
}