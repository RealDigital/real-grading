<?php


namespace App\Security;


class SecurityUtil
{
    static public function getToken($username)
    {
        $str = md5(uniqid(md5(microtime(true)),true));
        return sha1($str.$username);
    }

    static public function passCrypt($psw,$salt)
    {
        $psw=md5($psw.$salt);
        return $psw;
    }

    static public function saltGen()
    {
        return strval(rand(100,999));
    }
}