<?php


namespace App\Security;


class Result
{
    #1000～1999 区间表示参数错误
    ##2000～2999 区间表示用户错误
    ##3000～3999 区间表示接口异常
    const USERNAME_REPEAT = 2000;
    const EMAIL_REPEAT = 2001;
    const TOKEN_INVALID = 2002;
    const SUCCESS = 1;
    const ERROR = -1;

    public static $msg = [
        self::SUCCESS => 'SUCCESS',
        self::USERNAME_REPEAT => 'USERNAME_REPEAT',
        self::EMAIL_REPEAT => 'EMAIL_REPEAT',
        self::TOKEN_INVALID => 'TOKEN_INVALID',
        self::ERROR => 'ERROR',
    ];

    static public function return($code, $data = null)
    {

        return
            [
                'code' => $code,
                'msg' => self::$msg[$code],
                'data' => $data
            ];

    }
}